# Keycloak test (Java based demo)

Original article is located in https://developers.redhat.com/articles/2022/12/07/how-implement-single-sign-out-keycloak-spring-boot

## Components
- Spring Boot
- Spring Security
- Java 11
- Keycloak
  - address: http://localhost:8080 
  - realm: test-realm
  - client: test-client
  - client secret: _create in Keycloak_
- MySQL (for session persistence, for demo)
  - address: 127.0.0.1:3306 
  - database name: _db_session_
    To create
      ```CREATE DATABASE db_session;```

## Keycloak settings
1. Create realm test-realm
2. Create client test-client
3. Setup client

![Client settings](./docs/keycloak_client_settings.png)

4. Create user _test_ with password _test_

## Run

1. Use Idea or command line to build and run

Command line option
```bash
mvn clean package
java -jar target/keycloak-1.0-SNAPSHOT.jar
```

2. Open browser and go to http://localhost:8081
After provide credentials you will be redirected to base page with output

```json
{"hello":"test"}
```

3. Go to logout http://localhost:8081/logout
it will be redirected to keycloak
![img.png](docs/keycloak_logout.png)
4. Press on logout button
![img.png](docs/keycloak_logout02.png)
5. Go to http://localhost:8081 and observer you need to provide credentials
6. End

